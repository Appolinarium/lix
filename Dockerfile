FROM golang:1.21.1-alpine3.18

COPY . /lix
RUN apk update && apk upgrade 
RUN apk add make && apk add binutils
RUN cd /lix && make

CMD /lix/lix
