all:
	go build 
	strip -s ./lix

clean:
	rm ./lix

docs:
	swagger generate spec -i swagger.yml -o ./swagger.json

docs-ui:
	swagger serve -F swagger -p 3000 --no-open swagger.json