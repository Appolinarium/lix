# Lix

Modern ipify remake 

## Configuration

All config relies on `.env`. Available variables are:

Balancer/reverse-proxy client ip header to parse
```
CLIENT_IP_HEADER=""
```
Service listen ip address
```
LISTEN_ADDR=0.0.0.0
```
Service listen port
```
LISTEN_PORT=3000
```

## Build

```bash
make
```

## Deployment

Set required `CLIENT_IP_HEADER` in .env. After that, run:


```bash
docker-compose up -d
```

## Links

promo page - https://lix.sodafunk.ru

API - https://api.lix.sodafunk.ru

API documentation - https://docs.lix.sodafunk.ru