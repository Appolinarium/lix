package api


import (
	"net/http"
	"encoding/json"
	"lix/models"
	"github.com/go-chi/chi/v5"
	"lix/errors"
	"os"
)


func GetIP(w http.ResponseWriter, r *http.Request) {
	addr := r.Header.Get(os.Getenv("CLIENT_IP_HEADER"))
	format := chi.URLParam(r, "format")

	switch format {
		case "json":
			w.Header().Set("Content-Type", "application/json")
			json_format, _ := json.Marshal(models.IPAddress{addr})
			w.Write(json_format)
			return
		case "xml":
			w.Header().Set("Content-Type", "text/plain")
			w.Write([]byte("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<ip>" + addr + "</ip>"))
			return
		case "":
			w.Header().Set("Content-Type", "text/plain")
			w.Write([]byte(addr))
			return
		default:
			errors.FormatNotAllowed(w, r)
			return
	}

}
