package errors


import (
	"net/http"
)


func FormatNotAllowed(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("Format not allowed"))
}
