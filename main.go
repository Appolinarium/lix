package main


import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
	"lix/api"
	"fmt"
	"github.com/go-chi/cors"
	"os"
	"lix/utils"
)


func main() {
	r:= chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins: []string{"https://*", "http://*"},
		AllowedMethods: []string{"GET"},
	}))


	//	swagger:route GET /json json
	//
	//	Represent ip in JSON format
	//
    //	Consumes:
    //		- application/json
	//
    //     Responses:
    //       200:
	//			description: OK
	//
    //	Produces:
    //		- application/json

	// swagger:route GET /xml xml
	//
    //	Consumes:
    //		- text/plain
	//
	// Represent ip in XML format
	//
    //     Responses:
    //       200:
	//			description: OK
	//
    //     Produces:
    //     - text/plain

	r.Get("/{format:.*}", api.GetIP)

	// swagger:route GET / plain
	//
	// Represent ip in plain text format, non-cannonical path supported (without end slash) 
	//
    //	Consumes:
    //		- text/plain
	//
    //     Produces:
    //     - text/plain
	//
    //     Responses:
    //       200:
	//			description: OK
	r.Get("/", api.GetIP)


	addr := os.Getenv("LISTEN_ADDR")
	port := os.Getenv("LISTEN_PORT")
	if len(addr) <= 0 {
		addr = "0.0.0.0"
	}
	if len(port) <= 0 {
		port = "3000"
	}

	for _, e := range utils.ParseIPsPseudo(addr) {
		fmt.Println("Listen on http://" + e.String() + ":" + string(port))
	}

	http.ListenAndServe(addr + ":" + port, r)
}
