package utils

import (
	"net"
	"log"
)


func GetLocalIPs() ([]net.IP, error) {
    var ips []net.IP
    addresses, err := net.InterfaceAddrs()
    if err != nil {
        return nil, err
    }

    for _, addr := range addresses {
        if ipnet, ok := addr.(*net.IPNet); ok {
            if ipnet.IP.To4() != nil {
                ips = append(ips, ipnet.IP)
            }
        }
    }
    return ips, nil
}

func ParseIPsPseudo(IP string) []net.IP {
	var ips []net.IP
	if IP == "0.0.0.0" {
		ips, err := GetLocalIPs()
		if err != nil {
			log.Fatal(err)
		}
		return ips
	}
	ips = append(ips, net.ParseIP(IP))
    return ips
}